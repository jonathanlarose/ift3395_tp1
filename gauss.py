# coding=utf-8

import numpy
# Estimateur de densité paramétrique Gausienne diagonale

class est_dst_gauss_diag:
    
    def __init__(self, n_dims):
        #utile le n_dims?? si seulement diag
        self.n_dims = n_dims
        self.mu = np.empty((1, n_dims))
        self.sigma_sq = np.empty(n_dims)
    
    def train(self, train_data):
        self.mu = np.mean(train_data, axis=0)
        self.sigma_sq = numpy.sum((train_data - self.mu) ** 2.0, axis = 0) / train_data.shape[0]
    
    def compute_prediction(self, test_data):
        print self.mu
        print self.sigma_sq
        
        
        # Determinant de la matrice (diagonale) de covariance est le produit de sa diagonale
        det_cov = np.prod(self.sigma_sq)
        # Calcul du dénominateur (normalisation)
        cnorm = -(self.n_dims / 2.0 * np.log(2*np.pi) + (np.log(det_cov) / 2.0))
        # Calcul des log de probabilité des data tests
        log_prob = cnorm - np.sum(((test_data - self.mu)**2.0) / self.sigma_sq / 2.0, axis=1)
        
        return log_prob